# Classroom Countdown



## License

    Classroom Countdown
    Copyright 2016-2017 Jamie McClymont <jamie@kwiius.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*I can potentially be convinced to relicense to a more liberal license; let me know if you have a reasonable reason to need such a change.*

### Dependencies

This project incorporates or depends on the following, each available under
their own license(s):

#### Web Version

* `Moment` and `Moment-Timezone`
* `whatwg-fetch`
* Babel's js polyfill

#### Backend

* The python libraries `requests` and `ics`
* The `jq` command
