.PHONY: tarball
tarball: web backend

.PHONY: web
web:
	cd web && $(MAKE) tarball

.PHONY: backend
backend:
	cd backend && $(MAKE) tarball
