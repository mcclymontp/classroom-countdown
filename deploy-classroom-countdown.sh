#!/bin/bash
set -euf -o pipefail

cd /var/www/classroom-countdown
curl -L 'https://gitlab.com/JJJollyjim/classroom-countdown/builds/artifacts/master/raw/web/dist.tar.xz?job=build_web' | tar -xJf -
