local function date(tt)
	return string.format("%04u-%02u-%02u", tt.year, tt.month, tt.day)
end

local function wday(tt)
	if tt.wday == 1 then
		-- Sunday should be 7
		return 7
	else
		-- Monday to Saturday should be 1 through 6
		return tt.wday - 1
	end
end

return {
	date = date,
	wday = wday
}
