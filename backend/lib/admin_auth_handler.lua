local ck = require "cookie"
local admin_auth = require "admin_auth"

if not (admin_auth.get())[ck:new():get("cc_admin_token")] then
	ngx.say("access denied")
	ngx.exit(ngx.HTTP_FORBIDDEN)
end
