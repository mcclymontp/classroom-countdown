#!/usr/bin/env python3
"""Generates info.json from config and calendar"""

import icshandler
import configparser
import requests
import json


config = configparser.ConfigParser(interpolation=None, delimiters=('='))
config.read('/etc/classroom-countdown.ini')

if config.getboolean('main', 'manualoverride'):
    raise ValueError("Warning: manual override enabled, doing nothing")

ics = requests.get(config['main']['ics']).text

timetables = {k: dict(v) for k, v in config.items() if k[0] == "!"}

with open(config['weather']['store']) as file:
    weather = json.load(file)

print(icshandler.make_json(
    ics=ics,
    defaults=config['defaults'],
    timetables=timetables,
    tz=config['main']['tzstring'],
    weather=(
        (config['weather']['default'] if
         config.getboolean('weather', 'enabled') is True
         else None),
        weather
    )
))
