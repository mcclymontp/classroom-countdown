#!/bin/bash
set -euf -o pipefail

TMPJSON=/var/www/classroom-countdown/info.json.tmp
JSON=/var/www/classroom-countdown/info.json

cd /opt/ccdeploy

./main.py > $TMPJSON

diff --color=always <(jq -S . < $JSON) <(jq -S . < $TMPJSON) || true

mv $TMPJSON $JSON
