#!/bin/bash

TMPJSON=../web/dist/info.json.tmp
JSON=../web/dist/info.json

echo "Generating info.json"

./main.py > $TMPJSON

echo "Done. Showing differences for confirmation:"

sleep 2

diff --color=always <(jq -S . < $JSON) <(jq -S . < $TMPJSON)

read -r -p "Confirm these changes? [y/N] " response
case "$response" in
	[yY][eE][sS]|[yY]) 
		echo "Updating live version"
		mv $TMPJSON $JSON
		echo "Done!"
		sleep 2
		;;
	*)
		echo "No changes made."
		sleep 2
		;;
esac 
