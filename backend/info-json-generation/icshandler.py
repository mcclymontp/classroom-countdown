import itertools
from ics import Calendar
import datetime
import json


class SetOnceDict(dict):
    def __setitem__(self, key, value):
        if key in self:
            raise ValueError("SetOnceDict prohibits this")

        super().__setitem__(key, value)


def date_range(first, last):
    for x in range(0, (last - first).days):
        yield first + datetime.timedelta(days=x)


DAYS = {
    'mon': 1,
    'tue': 2,
    'wed': 3,
    'thu': 4,
    'fri': 5,

    'sat': 6,
    'sun': 7,  # ISO standard
}


def make_json(ics, defaults, timetables, tz, weather):
    cal = Calendar(ics)
    default_weather, weather_events = weather

    if len(cal.events) < 1:
        raise ValueError("Sanity check failed (no events in calendar?)")

    if set(defaults.keys()) != DAYS.keys():
        raise ValueError("Error in defaults secion of config file")

    obj = {
        "defaults": {DAYS[day]: spec for day, spec in defaults.items()},
        "days": SetOnceDict(),
        "timetables": {},
        "tz": tz,
        "weather": {"default": default_weather, "days": weather_events}
    }

    for event in cal.events:
        for day in date_range(event.begin.date(), event.end.date()):
            try:
                obj["days"][day.isoformat()] = event.name
            except ValueError:
                raise ValueError("Multiple events on one day")

        try:
            if day is None:
                raise ValueError("Issue with an event! Title is '{}'"
                                 .format(event.name))
        except NameError:
            raise ValueError("Non all-day event? Title is '{}'"
                             .format(event.name))

    for dayspec in itertools.chain(obj["days"].values(),
                                   obj["defaults"].values()):
        if dayspec[0] == "!":
            # Ensure that the timetable is sent
            obj["timetables"][dayspec] = timetables[dayspec]

    return json.dumps(obj, sort_keys=True)
